package org.F.controller;


import org.F.model.Categoria;
import org.F.response.CategoriaResponseRest;
import org.F.service.ICategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:4200/"})
@RestController
@RequestMapping("/v1")
public class CategoriaController {

    @Autowired
    private ICategoriaService service;

    @GetMapping("/categorias")
    public ResponseEntity<CategoriaResponseRest> allCategorias(){
        ResponseEntity<CategoriaResponseRest> response = service.obtenerCategorias();
        return response;
    }

    @CrossOrigin(origins = "http://localhost:4200/")
    @PostMapping("/categorias")
    public ResponseEntity<CategoriaResponseRest> crearCategoria(Categoria request){
        ResponseEntity<CategoriaResponseRest> response = service.crearCategoria(request);
        return response;
    }

}
