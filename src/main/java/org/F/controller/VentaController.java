package org.F.controller;

import org.F.model.Venta;
import org.F.response.VentaResponseRest;
import org.F.service.IVentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:4200/"})
@RestController
@RequestMapping("/v1")
public class VentaController {

    @Autowired
    private IVentaService service;

    @GetMapping("/ventas")
    @Transactional(readOnly = true)
    public ResponseEntity<VentaResponseRest> obtenerVentas(){
        ResponseEntity<VentaResponseRest> response = service.obtenerVentas();
        return response;
    }

    @PostMapping("/ventas")
    @Transactional
    public ResponseEntity<VentaResponseRest> crearVenta(@RequestBody Venta request){
        ResponseEntity<VentaResponseRest> response = service.crearVenta(request);
        return response;
    }

}
