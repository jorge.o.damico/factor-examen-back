package org.F.controller;

import org.F.model.Producto;
import org.F.response.ProductoResponseRest;
import org.F.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1")
public class ProductoController {

    @Autowired
    private IProductoService service;

    @GetMapping("/productos")
    public ResponseEntity<ProductoResponseRest> obtenerProductos(){
        ResponseEntity<ProductoResponseRest> response = service.obtenerProductos();
        return response;
    }

    @PostMapping("/productos")
    public ResponseEntity<ProductoResponseRest> crearProducto(@RequestBody Producto request){
        ResponseEntity<ProductoResponseRest> response = service.crearProducto(request);
        return response;
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<ProductoResponseRest> eliminarProducto(@PathVariable Long id){
        ResponseEntity<ProductoResponseRest> response = service.eliminarProducto(id);
        return response;
    }

}
