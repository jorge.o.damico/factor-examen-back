package org.F.controller;

import org.F.model.Usuario;
import org.F.response.UsuarioResponseRest;
import org.F.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:4200/"})
@RestController
@RequestMapping("/v1")
public class UsuarioController {

    @Autowired
    private IUsuarioService service;

    @GetMapping("/usuarios")
    public ResponseEntity<UsuarioResponseRest> consultarUsuarios() {

        ResponseEntity<UsuarioResponseRest> response = service.obtenerUsuarios();
        return response;

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/usuarios")
    public ResponseEntity<UsuarioResponseRest> crearUsuario(@RequestBody Usuario request){

        if(request.getUserName() == null){
            request.setUserName("");
        }
        if(request.getPassword() == null){
            request.setPassword("");
        }

        ResponseEntity<UsuarioResponseRest> response = service.crearUsuario(request);
        return response;

    }

}
