package org.F.service;

import org.F.model.Role;
import org.F.model.Usuario;
import org.F.model.dao.IUsuarioDao;
import org.F.response.UsuarioResponseRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioService implements UserDetailsService, IUsuarioService {

    private static final Logger log = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private IUsuarioDao usuarioDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public UsuarioService(IUsuarioDao usuarioDao, BCryptPasswordEncoder passwordEncoder) {
        this.usuarioDao = usuarioDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Usuario usuario = this.usuarioDao.findByUserName(username);
        
        if( usuario == null) {
            log.error("Error, el usuario no existe");
            throw new UsernameNotFoundException("Error, el usuario no existe");
        }

        List<GrantedAuthority> authorities = usuario.getRoles()
                .stream()
                .map( role -> new SimpleGrantedAuthority(role.getRoleName()))
                .peek( authority -> log.info("Role: "+ authority.getAuthority()))
                .collect(Collectors.toList());

        return new User(usuario.getUserName(), usuario.getPassword(), usuario.getHabilitado(), true, true, true, authorities);
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<UsuarioResponseRest> obtenerUsuarios() {

        log.info("Inicio del método obtener usuarios");

        UsuarioResponseRest response = new UsuarioResponseRest();

        try {
            List<Usuario> userList = (List<Usuario>) usuarioDao.findAll();
            response.getUsuarioResponse().setUsuario(userList);
            response.setMetadata("Response OK", "0", "Respuesta exitosa");
        } catch (Exception e) {
            log.error("Exception al buscar users");
            e.printStackTrace();
            response.setMetadata("Respuesta nok", "-1", "Exception al buscar users");
            return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.OK);

    }

    @Override
    @Transactional
    public ResponseEntity<UsuarioResponseRest> crearUsuario(Usuario usuario) {

        log.info("Inicio del método crear Usuario");

        UsuarioResponseRest response = new UsuarioResponseRest();

        List<Usuario> listaUsuarios = new ArrayList<>();

        usuario.setPassword(usuario.getPassword().trim());
        usuario.setUserName(usuario.getUserName().trim());
        usuario.setPassword(usuario.getPassword().replace(" ", ""));
        usuario.setUserName(usuario.getUserName().replace(" ", ""));

        if(usuario.getUserName().trim().isEmpty()){
            log.info("Debe ingresar un usuario");
            response.setMetadata("Debe ingresar un usuario", "-1", "Campo obligatorio");
            return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.BAD_REQUEST);
        } else if (usuario.getUserName().length() < 5 || usuario.getUserName().length() > 20) {
            log.info("El usuario debe tener entre 5 y 20 caracteres");
            response.setMetadata("El usuario debe tener entre 5 y 20 caracteres", "-1",
                    "El usuario debe tener entre 5 y 20 caracteres");
            return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.BAD_REQUEST);
        }

        if(usuario.getPassword().trim().isEmpty()){
            log.info("Debe ingresar un Password");
            response.setMetadata("Debe ingresar un Password", "-1", "Campo obligatorio");
            return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.BAD_REQUEST);
        } else if (usuario.getPassword().length() < 8 || usuario.getPassword().length() > 20) {
            log.info("El password debe tener entre 8 y 20 caracteres");
            response.setMetadata("El password debe tener entre 8 y 20 caracteres", "-1",
                    "El password debe tener entre 8 y 20 caracteres");
            return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.BAD_REQUEST);
        }

        try {

            String password = passwordEncoder.encode(usuario.getPassword());
            usuario.setPassword(password);

            Usuario buscarUsuario = usuarioDao.findByUserName(usuario.getUserName());

            if( buscarUsuario != null ){
                log.info("El usuario" + " " + buscarUsuario.getUserName() + " " + "ya existe");
                response.setMetadata("El usuario " + " " +
                        buscarUsuario.getUserName() + " " + " ya existe", "-1", "Respuesta fallida");
                return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            Usuario usuarioNuevo = usuarioDao.save(usuario);

            if(usuarioNuevo != null) {
                usuarioNuevo.setHabilitado(true);

                Role role = new Role();
                role.setRoleName("user");
                role.setId(Long.valueOf(1));
                List<Role> roleList = new ArrayList<>();
                roleList.add(role);
                usuarioNuevo.setRoles(roleList);

                listaUsuarios.add(usuarioNuevo);
                response.getUsuarioResponse().setUsuario(listaUsuarios);
                response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
            } else {
                log.error("Error al crear usuario");
                response.setMetadata("Respuesta nok", "-1", "Error al grabar usuario");
                return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            log.error("Exception al grabar user");
            response.setMetadata("Respuesta nok", "-1", "Exception al grabar user");
            return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<UsuarioResponseRest>(response, HttpStatus.OK);

    }

}
