package org.F.service;

import org.F.model.Venta;
import org.F.response.VentaResponseRest;
import org.springframework.http.ResponseEntity;

public interface IVentaService {

    public ResponseEntity<VentaResponseRest> obtenerVentas();

    public ResponseEntity<VentaResponseRest> crearVenta(Venta venta);

}
