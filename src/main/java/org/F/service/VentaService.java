package org.F.service;

import org.F.model.Usuario;
import org.F.model.Venta;
import org.F.model.dao.IVentaDao;
import org.F.response.UsuarioResponseRest;
import org.F.response.VentaResponseRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VentaService implements IVentaService{


    private static final Logger log = LoggerFactory.getLogger(VentaService.class);

    @Autowired
    private IVentaDao ventaDao;

    @Override
    public ResponseEntity<VentaResponseRest> obtenerVentas() {

        log.info("Inicio del método obtener ventas");

        VentaResponseRest response = new VentaResponseRest();

        try {
            List<Venta> ventaList = (List<Venta>) ventaDao.findAll();
            ventaList.stream().forEach(x -> x.getUsuario().setPassword(""));
            response.getVentaResponse().setVenta(ventaList);
            response.setMetadata("Response OK", "0", "Respuesta exitosa");
        } catch (Exception e) {
            log.error("Exception al buscar venta");
            e.printStackTrace();
            response.setMetadata("Respuesta nok", "-1", "Exception al buscar venta");
            return new ResponseEntity<VentaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<VentaResponseRest>(response, HttpStatus.OK);

    }

    @Override
    public ResponseEntity<VentaResponseRest> crearVenta(Venta venta) {

        log.info("Inicio del método crear venta");

        VentaResponseRest response = new VentaResponseRest();

        List<Venta> ventaList = new ArrayList<>();

        try {
            Venta ventaNueva = ventaDao.save(venta);
            if(ventaNueva != null){
                ventaList.add(ventaNueva);
                response.getVentaResponse().setVenta(ventaList);
                response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
            } else {
                log.error("Error al crear venta");
                response.setMetadata("Respuesta nok", "-1", "Error al grabar venta");
                return new ResponseEntity<VentaResponseRest>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Exception al grabar venta");
            response.setMetadata("Respuesta nok", "-1", "Exception al grabar venta");
            return new ResponseEntity<VentaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<VentaResponseRest>(response, HttpStatus.OK);
    }

}
