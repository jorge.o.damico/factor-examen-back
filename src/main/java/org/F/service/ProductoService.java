package org.F.service;

import org.F.model.Producto;
import org.F.model.dao.IProductoDao;
import org.F.response.CategoriaResponseRest;
import org.F.response.ProductoResponseRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductoService implements IProductoService{

    private static final Logger log = LoggerFactory.getLogger(ProductoService.class);

    @Autowired
    private IProductoDao productoDao;

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<ProductoResponseRest> obtenerProductos() {

        log.info("Inicio método obtener todos los productos");

        ProductoResponseRest response = new ProductoResponseRest();

        try{
            List<Producto> producto = (List<Producto>) productoDao.findAll();
            response.getProductoResponse().setProducto(producto);
            response.setMetadata("Respuesta OK", "00", "Respuesta exitosa");
        } catch (Exception e) {
            response.setMetadata("Respuesta fallida", "-1", "Error al obtener productos");
            log.error("Error al obtener productos", e.getMessage());
            e.getStackTrace();
        }

        return new ResponseEntity<ProductoResponseRest>(response, HttpStatus.OK);

    }

    @Override
    @Transactional
    public ResponseEntity<ProductoResponseRest> crearProducto(Producto producto) {

        log.info("Inicio del método crear producto");

        ProductoResponseRest response = new ProductoResponseRest();

        List<Producto> listaProductos = new ArrayList<>();

        try{
            Producto productoNuevo = productoDao.save(producto);
            if(productoNuevo != null){
                listaProductos.add(productoNuevo);
                response.getProductoResponse().setProducto(listaProductos);
                response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
            } else {
                log.error("Error al crear producto");
                response.setMetadata("Respuesta nok", "-1", "Error al grabar producto");
                return new ResponseEntity<ProductoResponseRest>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Exception al grabar producto");
            response.setMetadata("Respuesta nok", "-1", "Exception al grabar producto");
            return new ResponseEntity<ProductoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<ProductoResponseRest>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProductoResponseRest> eliminarProducto(Long id) {

        log.info("Inicio del método eliminar producto");

        ProductoResponseRest response = new ProductoResponseRest();

        try {
            productoDao.deleteById(id);
            response.setMetadata("Respuesta ok", "00", "Producto eliminado");
        } catch (Exception e) {
            log.error("Error en eliminar el producto", e.getMessage());
            e.getStackTrace();
            response.setMetadata("Respuesta nok", "-1", "Producto no eliminado");
            return new ResponseEntity<ProductoResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<ProductoResponseRest>(response, HttpStatus.OK);
    }

}
