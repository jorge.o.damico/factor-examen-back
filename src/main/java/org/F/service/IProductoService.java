package org.F.service;

import org.F.model.Producto;
import org.F.response.ProductoResponseRest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface IProductoService {

    public ResponseEntity<ProductoResponseRest> obtenerProductos();

    public ResponseEntity<ProductoResponseRest> crearProducto(Producto producto);

    public ResponseEntity<ProductoResponseRest> eliminarProducto(Long id);

}
