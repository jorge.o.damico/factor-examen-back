package org.F.service;

import org.F.model.Usuario;
import org.F.response.UsuarioResponseRest;
import org.springframework.http.ResponseEntity;

public interface IUsuarioService {

    public ResponseEntity<UsuarioResponseRest> obtenerUsuarios();

    public ResponseEntity<UsuarioResponseRest> crearUsuario(Usuario usuario);

}
