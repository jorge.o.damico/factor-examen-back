package org.F.service;

import org.F.model.Categoria;
import org.F.model.dao.ICategoriaDao;
import org.F.response.CategoriaResponseRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoriaService implements ICategoriaService {

    private static final Logger log = LoggerFactory.getLogger(CategoriaService.class);

    @Autowired
    private ICategoriaDao categoriaDao;

    @Override
    public ResponseEntity<CategoriaResponseRest> obtenerCategorias() {

        log.info("Inicio del método todas las categorías");

        CategoriaResponseRest response = new CategoriaResponseRest();

        try {
            List<Categoria> listaCategorias = (List<Categoria>) categoriaDao.findAll();
            response.getCategoriaResponse().setCategoria(listaCategorias);
            response.setMetadata("Response OK", "0", "Respuesta exitosa");
        } catch (Exception e){
            log.error("Exception al buscar categorias");
            e.printStackTrace();
            response.setMetadata("Respuesta nok", "-1", "Exception al buscar categorias");
            return new ResponseEntity<CategoriaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<CategoriaResponseRest>(response, HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<CategoriaResponseRest> crearCategoria(Categoria categoria) {

        log.info("Inicio del método crear categoría");

        CategoriaResponseRest response = new CategoriaResponseRest();

        List<Categoria> listaCategorias = new ArrayList<>();

        try {
            Categoria categoriaNueva = categoriaDao.save(categoria);
            if(categoriaNueva != null){
                listaCategorias.add(categoriaNueva);
                response.getCategoriaResponse().setCategoria(listaCategorias);
                response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
            } else {
                log.error("Error al crear categoria");
                response.setMetadata("Respuesta nok", "-1", "Error al grabar categoria");
                return new ResponseEntity<CategoriaResponseRest>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Exception al grabar categoria");
            response.setMetadata("Respuesta nok", "-1", "Exception al grabar categoria");
            return new ResponseEntity<CategoriaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<CategoriaResponseRest>(response, HttpStatus.OK);
    }

}
