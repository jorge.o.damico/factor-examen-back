package org.F.service;

import org.F.model.Categoria;
import org.F.response.CategoriaResponseRest;
import org.springframework.http.ResponseEntity;

public interface ICategoriaService {

    public  ResponseEntity<CategoriaResponseRest> obtenerCategorias();

    public ResponseEntity<CategoriaResponseRest> crearCategoria(Categoria categoria);

}
