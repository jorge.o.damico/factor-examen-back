package org.F.response;

import org.F.model.Venta;

import java.util.List;

public class VentaResponse {

    private List<Venta> venta;

    public List<Venta> getVenta() {
        return venta;
    }

    public void setVenta(List<Venta> venta) {
        this.venta = venta;
    }

}
