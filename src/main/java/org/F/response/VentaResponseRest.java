package org.F.response;

public class VentaResponseRest extends ResponseRest{

    private VentaResponse ventaResponse = new VentaResponse();

    public VentaResponse getVentaResponse() {
        return ventaResponse;
    }

    public void setVentaResponse(VentaResponse ventaResponse) {
        this.ventaResponse = ventaResponse;
    }

}
