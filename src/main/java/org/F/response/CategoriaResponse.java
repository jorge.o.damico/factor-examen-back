package org.F.response;

import org.F.model.Categoria;

import java.util.List;

public class CategoriaResponse {

    private List<Categoria> categoria;

    public List<Categoria> getCategoria() {
        return categoria;
    }

    public void setCategoria(List<Categoria> categoria) {
        this.categoria = categoria;
    }

}
