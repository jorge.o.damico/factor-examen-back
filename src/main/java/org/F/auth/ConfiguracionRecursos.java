package org.F.auth;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableResourceServer
public class ConfiguracionRecursos extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/v1/usuarios").permitAll()
                .antMatchers(HttpMethod.POST, "/v1/usuarios").permitAll()
                .antMatchers(HttpMethod.GET, "/v1/categorias").permitAll()
                .antMatchers(HttpMethod.POST, "/v1/categorias").permitAll()
                .antMatchers(HttpMethod.GET, "/v1/productos").permitAll()
                .antMatchers(HttpMethod.POST, "/v1/productos").permitAll()
                .antMatchers(HttpMethod.DELETE, "/v1/productos/{id}").permitAll()
                .antMatchers(HttpMethod.POST, "/v1/ventas").permitAll()
                .antMatchers(HttpMethod.GET, "/v1/ventas").permitAll()
                .anyRequest().authenticated()
                .and().cors().configurationSource(corsConfigurationSource()); //Saltar cors local
    }

    //Saltar cors local
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {

        CorsConfiguration config = new CorsConfiguration();

        config.setAllowedOrigins(Arrays.asList("http://localhost:4200"));//Arrays.asList("http://localhost:4200")
        //config.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        config.setAllowCredentials(true);
        config.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization", "Custom_Header"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        source.registerCorsConfiguration("/**", config);

        return source;

    }

}
