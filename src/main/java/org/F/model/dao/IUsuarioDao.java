package org.F.model.dao;

import org.F.model.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {

    public Usuario findByUserName(String username);

    @Query("select u from Usuario u where u.userName=?1")
    public Usuario findUser(String userName);


}
