package org.F.model.dao;

import org.F.model.Venta;
import org.springframework.data.repository.CrudRepository;

public interface IVentaDao extends CrudRepository<Venta, Long> {

}
