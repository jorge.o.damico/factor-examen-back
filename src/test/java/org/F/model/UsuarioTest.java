package org.F.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UsuarioTest {

    @Test
    void userNameTest(){
        Usuario user = new Usuario();
        user.setUserName("Jorge");
        String esperado = "Jorge";
        assertEquals(esperado, user.getUserName());
    }

}