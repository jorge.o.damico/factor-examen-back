package org.F.controller;

import org.junit.jupiter.api.Test;
import org.F.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.*;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class UsuarioControllerTest {

    @Autowired(required = false)
    private WebTestClient client;

    @Test
    void consultarUsuarios() {
        client.get().uri("http://localhost:5000/v1/usuarios")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON);
    }

    @Test
    void crearUsuario() {
        Usuario user = new Usuario();
        user.setUserName("Prueba11");
        user.setPassword("Prueba12345678");
        client.post().uri("http://localhost:5000/v1/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(user)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.metadata").isNotEmpty();

    }

}