package org.F.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.F.model.dao.IUsuarioDao;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.mockito.Mockito.mock;

@SpringBootTest
class UsuarioServiceTest {

    IUsuarioDao usuarioDao;

    UsuarioService service;

    BCryptPasswordEncoder encoder;

    @BeforeEach
    void setUp(){
        usuarioDao = mock(IUsuarioDao.class);
        encoder = mock(BCryptPasswordEncoder.class);
        service = new UsuarioService(usuarioDao, encoder);
    }

    @Test
    void loadUserByUsernameTest() {

    }

    @Test
    void obtenerUsuariosTest() {

    }

    @Test
    void crearUsuarioTest() {

    }

}